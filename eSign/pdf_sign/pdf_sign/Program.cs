﻿using Microsec.XSign4Net;
using Microsec.XSign4Net.PDF;
using System;
using System.Security;
using System.Diagnostics;
using System.IO;

namespace pdf_sign
{
    class Program
    {
        static int Main(string[] args)
        {
            string InputPdf = args[0];
            string OutputPdf = args[1];

            //string InputPdf = "in.pdf";
            //string OutputPdf = "out.pdf";

            //This is an example of creating pdf signature and timestamp.
            try
            {
                using (XSignSession xsign = new XSignSession())
                {
                    //Initialize logging
                    //xsign.SetLogFilePath("./log.txt");
                    xsign.SetLogFilePathAndLevel("./log.txt", XS_DEBUG_LEVEL.MY_DEBUG_LEVEL_ERROR);

                    //Initialize xsign
                    xsign.Initialize(".", "server_reg.xml", string.Empty);

                    //Dont show dialogs
                    xsign.SetWorkSilent(WorkMode.XS_FORCE_SILENT);

                    //Set signing certificate
                    SecureString pwd = new SecureString();
                    pwd.AppendChar('1');
                    pwd.AppendChar('2');
                    pwd.AppendChar('3');
                    pwd.AppendChar('4');
                    pwd.AppendChar('5');
                    xsign.SetSigningCertificateFile("peterke.pfx", pwd);
                    //xsign.SetSigningCertificateFile("vhid.pfx", pwd); // ÉLES BEÁLLÍTÁS

                    //Set XAdES-T and newest XAdES version
                    xsign.SetAdESType(XSignAdesType.XS_ADES_T);
                    xsign.SetXAdESVersion(XAdESVersion.XS_XADES_VERSION_EN_1_0_0);

                    //Set the revocation checking mode to OCSP
                    xsign.SetRevocationCheckingMode(RevocationCheckingMode.XS_REV_CHECK_OCSP);

                    //Set the timestamp service URL. 
                    xsign.SetTimeStampURLs(new string[] { "https://bteszt.e-szigno.hu/tsa" });
                    //xsign.SetTimeStampURLs(new string[] { "https://btsa.e-szigno.hu/tsa" }); // ÉLES BEÁLLÍTÁS

                    //Set basic authentication data
                    xsign.SetBasicAuthList(new HostBasicAuthData[] { new HostBasicAuthData(
                        "bteszt.e-szigno.hu",       //The host name
                        "teszt",                    //The username
                        "teszt"                     //The password
                    ) });
                    /*xsign.SetBasicAuthList(new HostBasicAuthData[] { new HostBasicAuthData(
                        "btsa.e-szigno.hu",       //The host name
                        "3.2.5885",                    //The username
                        "aib7Ohgh"                     //The password
                    ) });*/ // ÉLES BEÁLLÍTÁS

                    //Create a new pdf
                    using (PdfDocument pdf = xsign.OpenPdfDocument(InputPdf))
                    {
                        //Sign it
                        PdfSignature sig = pdf.Sign();

                        //Save it
                        pdf.Save(OutputPdf);
                    }

                    //Close log file
                    xsign.CloseLogFilePath();
                }
            }
            catch (Exception ex)
            {
                System.Console.Error.WriteLine(ex.StackTrace);
                Environment.Exit(-1);
            }

            return 0;
        }
    }
}
