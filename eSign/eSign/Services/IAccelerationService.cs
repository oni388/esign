﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using eSign.Controllers;

namespace eSign.Models
{
    [ServiceContract]
    public interface IAccelerationService
    {
        [OperationContract]
        Result eSignPdf(string xml);
    }
}