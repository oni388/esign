﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eSign.Models;
using eSign.Controllers;
using Microsoft.Extensions.Configuration;

namespace eSign.Services
{
    public class AccelerationService : IAccelerationService
    {
        IConfiguration _config;
        public AccelerationService(IConfiguration config)
        {
            _config = config;
        }

        public Result eSignPdf(string xml)
        { 
            return new eSignController(_config).eSignFunction(xml);
        }
    }
}