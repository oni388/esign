﻿using eSign.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Spire.Pdf;
using Spire.Pdf.Graphics;
using System.Drawing;
using Spire.Pdf.Attachments;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace eSign.Controllers
{
    public class Document
    {
        public string DocumentType { get; set; }
        public string InvoicePdfName { get; set; }
        public string InvoicePdf { get; set; }
        public string InvoiceXmlName { get; set; }
        public string InvoiceXml { get; set; }
        public List<Attachement> Attachements { get; set; } = new List<Attachement>();
    }

    public class Attachement
    {
        public string AttachmentID { get; set; }
        public string AttachmentPdfName { get; set; }
        public string AttachmentPdf { get; set; }
    }

    public class Result
    {
        public bool Success { get; set; } = true;
        public string Message { get; set; }
    }

    public class eSignController : Controller
    {
        private readonly string PdfTempFolder;
        private readonly string PdfSignFolder;

        public eSignController(IConfiguration config)
        {
            PdfTempFolder = config.GetValue<string>("CustomAppSettings:PdfTempFolder");
            PdfSignFolder = config.GetValue<string>("CustomAppSettings:PdfSignFolder");
        }
        
        public Result eSignFunction(string XMLContent)
        {
            try
            {
                // Directories
                string UnixTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString();
                string FullPath = Path.Combine(PdfTempFolder, UnixTimestamp);
                string FullPathPdf = Path.Combine(FullPath, "pdf");
                string FullPathAttachments = Path.Combine(FullPath, "attachments");
                string FullPathOutput = Path.Combine(FullPath, "output");
                string FullPathEszigno = Path.Combine(FullPath, "eszigno");

                // Create Directories
                Directory.CreateDirectory(FullPathPdf);
                Directory.CreateDirectory(FullPathAttachments);
                Directory.CreateDirectory(FullPathOutput);
                Directory.CreateDirectory(FullPathEszigno);

                // PostContent to Document Object
                string xmlContent = string.IsNullOrEmpty(XMLContent) ? "" : XMLContent.ToString();
                LogWrite(xmlContent);
                Document document = Deserialize<Document>(xmlContent.Trim());

                // Save InvoicePdf
                System.IO.File.WriteAllBytes(@Path.Combine(FullPathPdf, document.InvoicePdfName), Convert.FromBase64String(document.InvoicePdf));

                // Save InvoiceXml
                System.IO.File.WriteAllBytes(@Path.Combine(FullPathAttachments, document.InvoiceXmlName), Convert.FromBase64String(document.InvoiceXml));

                // Save Attachements
                foreach (Attachement attachement in document.Attachements) {
                    System.IO.File.WriteAllBytes(@Path.Combine(FullPathAttachments, attachement.AttachmentPdfName), Convert.FromBase64String(attachement.AttachmentPdf));
                }

                // Add attachements to pdf
                PdfDocument doc = new PdfDocument(Path.Combine(FullPathPdf, document.InvoicePdfName));
                DirectoryInfo d = new DirectoryInfo(@FullPathAttachments);
                FileInfo[] Files = d.GetFiles("*");
                foreach (FileInfo file in Files)
                {
                    PdfAttachment attachment = new PdfAttachment(Path.Combine(FullPathAttachments, file.Name));
                    doc.Attachments.Add(attachment);
                }
                doc.SaveToFile(Path.Combine(FullPathOutput, document.InvoicePdfName));

                // Sign Pdf
                Process ExternalProcess = new Process();
                ExternalProcess.StartInfo.FileName = Path.Combine(PdfSignFolder, "pdf_sign.exe");
                ExternalProcess.StartInfo.WorkingDirectory = Path.Combine(PdfSignFolder);
                ExternalProcess.StartInfo.Arguments = FullPathOutput + "\\" + document.InvoicePdfName + " " + FullPathEszigno + "\\" + document.InvoicePdfName;
                ExternalProcess.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                LogWrite("Start: " + ExternalProcess.StartInfo.FileName.ToString() + " " + ExternalProcess.StartInfo.Arguments.ToString());
                ExternalProcess.Start();
                ExternalProcess.WaitForExit();
                LogWrite("End: " + ExternalProcess.StartInfo.FileName.ToString() + " " + ExternalProcess.StartInfo.Arguments.ToString());

                byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(FullPathEszigno, document.InvoicePdfName));
                return new Result()
                {
                    Success = true,
                    Message = Convert.ToBase64String(fileBytes)
                };

                // Remove Directory
                //DirectoryInfo dir = new DirectoryInfo(FullPath);
                //dir.Delete(true);
                
            }
            catch (Exception ex)
            {
                System.Console.Error.WriteLine(ex.StackTrace);
                LogWrite(ex.StackTrace);
                LogWrite(ex.Message);
                return new Result()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }

        #region LogWrite
        public void LogWrite(string logMessage)
        {
            string m_exePath = PdfTempFolder;
            using (StreamWriter w = System.IO.File.AppendText(m_exePath + "log.txt"))
            {
                Log(logMessage, w);
            }
        }
        #endregion

        #region Log
        public void Log(string logMessage, TextWriter txtWriter)
        {
            txtWriter.WriteLine("{0} {1}: {2}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), logMessage);
            //txtWriter.WriteLine("-------------------------------");
        }
        #endregion

        #region Deserialize
        public T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
        #endregion

        #region Serialize
        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
        #endregion

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
